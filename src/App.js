import React, { Component } from 'react';
import ItemsList from './containers/ItemsList';
import SearchInput from './containers/SearchInput';
import FruitsFilter from './containers/FruitsFilter';

class App extends Component {
  render() {
    return (
      <div className="App">
        <FruitsFilter />
        <SearchInput />
        <ItemsList />
      </div>
    );
  }
}

export default App;
