import React from 'react';
import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import ReactDOM from 'react-dom';
import thunkMiddleware from 'redux-thunk';
import { Provider } from 'react-redux';
import items from './reducers/storeReducer';
import filters from './reducers/filtersReducer';
import App from './App';
import logger from './middlewares/logger';
import './index.css';

let reducers = combineReducers(
  {
    items,
    filters
  }
)

let store = createStore(
  reducers,
  compose(
    applyMiddleware(thunkMiddleware, logger),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);

ReactDOM.render(
  <Provider store={store}>
  <App />
  </Provider>,
  document.getElementById('root')
);
