import ActionTypes from './../constants/ActionTypes';

export function changeRequestState(actionType, payload) {
    return {
        type: actionType,
        payload: payload
    }
}

export function fetchItems() {
    return (dispatch) => {
        dispatch(changeRequestState(ActionTypes.FETCH_ITEMS_REQUEST));
        fetch('http://demo7666214.mockable.io/items')
            .then((response)=>response.json()).then((json) => {
                dispatch(changeRequestState(ActionTypes.FETCH_ITEMS_SUCCESS, json.items));
            })
            .catch(err => dispatch(changeRequestState(ActionTypes.FETCH_ITEMS_FAILURE)));
    }
}

export function search(query) {
    return {
        type: ActionTypes.SEARCH,
        payload: {
            query
        }
    }
}