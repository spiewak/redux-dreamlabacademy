export const logger = store => next => action => {
    if (action.type.endsWith('_REQUEST')) {
        console.info(`Sent async request ${action.type}`);
    }
    return next(action);
}

export default logger;