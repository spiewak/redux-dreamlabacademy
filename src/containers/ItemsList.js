import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Item from './../components/Item';
import { fetchItems } from './../actions/storeActions';


export class ItemsList extends Component {
    render() {
        return (
            <div>
                {this.props.items.map((item) => {
                    return  <Item id={item.id} name={item.name} desc={item.desc} key={item.id} />
                })}
            </div>
        )
    }

    componentDidMount() {
        this.props.fetchItems();
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.items
    }
}

const mapDispatchToProps = (dispach) => {
    return bindActionCreators({fetchItems}, dispach);
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemsList);