import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { search } from './../actions/storeActions';

export class SearchInput extends Component {
    constructor(props) {
        super(props);
        this.handleSearchQuery = this.handleSearchQuery.bind(this);
    }
    
    handleSearchQuery(event) {
        this.props.search(event.target.value);
    }

    render() {
        return (
            <input type="text" onChange={this.handleSearchQuery}/>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({search}, dispatch);
}

export default connect(null, mapDispatchToProps)(SearchInput);