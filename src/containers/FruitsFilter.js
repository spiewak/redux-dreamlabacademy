import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { filterFruits } from './../actions/filtersActions';

export class FruitsFilter extends React.Component {
    constructor(props) {
        super(props);
        this.handleOnClick = this.handleOnClick.bind(this);
    }

    handleOnClick() {
        this.props.filterFruits();
    }
    
    render() {
        return (
            <button onClick={this.handleOnClick}>Tylko owoce</button>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({filterFruits}, dispatch);
}

export default connect(null, mapDispatchToProps)(FruitsFilter);