import ActionTypes from './../constants/ActionTypes';


const initialState = {
    fruits: false
}

export const filterReducer = (state=initialState, action) => {
    switch(action.type) {
        case ActionTypes.FiLTER_FRUITS: {
            const newState = Object.assign({}, state, {fruits: !state.fruits});
            return newState;
        }
        default:
            return state;
    }
}

export default filterReducer;