import ActionTypes from './../constants/ActionTypes';

const initialState = [
    {
        id: 1,
        name: 'Jabłka',
        desc: 'Tanie, dobre, polskie',
        type: 'owoc',
        country: 'Polska'
    },
    {
        id: 2,
        name: 'Ziemniaki',
        desc: 'Idealne do obiadu',
        type: 'owoc',
        country: 'Polska'
    },
    {
        id: 3,
        name: 'Czereśnie',
        desc: 'Słodkie i dobre',
        type: 'owoc',
        country: 'Polska'
    },
    {
        id: 4,
        name: 'Czerwona kapusta',
        desc: 'Na drugie danie',
        type: 'warzywo',
        country: 'Polska'
    }
]

const storeReducer = (state=initialState, action) => {
    switch(action.type) {
        case ActionTypes.SEARCH: {
            const newState = state.filter((item) => {
                return item.name.toLowerCase().includes(action.payload.query);
            })
            return newState
        }
        case ActionTypes.FiLTER_FRUITS: {
            const newState = state.filter((item) => {
                return item.type === 'owoc';
            });
            return newState;
        }
        case ActionTypes.FETCH_ITEMS_SUCCESS: {
            return action.payload;
        }
        default: {
            return state
        }
    }
}

export default storeReducer;